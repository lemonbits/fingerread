# 42 char at row
# 19 chars in a product description
# 7 chars in cant
import sys
from escpos import *

POS = printer.File("/dev/usb/lp0")
amount = 0
subtotal = 0.0
total = 0.0
date = ''
internName = ''
product = []
quantity = []
price = []
count = 0

for line in sys.stdin:
	if count == 0:
		product = line.split('-')
		count += 1
	elif count == 1:
		quantity =  [int(elem) for elem in line.split('-')]
		count +=1
	elif count == 2:
		price =  [float(elem) for elem in line.split('-')]
		count +=1
	elif count == 3:
		date = line
		count +=1
	else:
		internName = line

POS.set('CENTER')
POS.text('Fecha: %s' %date)
POS.text('Nombre: %s\n' %internName)
POS.text('----------------------------------------\n')
POS.set('LEFT')
POS.text('Producto           Cant.  Precio  Subtotal\n')


for index in range(len(product)):
	amount = quantity[index]
	prod = product[index]
	if len(prod) >= 16:
		prod = prod[:12] + '...'
	if len(prod) < 16:
		aux = 16 - len(prod)
		prod += ' ' * aux
	pr = price[index]
	subtotal = amount * pr
	total += subtotal
	POS.text(prod + '   ' + str(amount) + '      ' + '{:05.2f}'.format(pr) + '     ' + str(subtotal) +'\n')
	# POS.text(str(amount) + ' pz. ' + prod +'\n')
	# POS.text('Prc Unit: '+ str(pr) + '\n')
	# + '	Subtotal:' + str(subtotal) + '\n')	

POS.text('----------------------------------------\n')
POS.set('RIGHT')
POS.text('Total: ' + str(total))

POS.cut()

print total
