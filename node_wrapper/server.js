var express = require('express');
var app = express();
var http = require('http').createServer(app);
var ddp = require('ddp');
var bodyParser = require('body-parser');
// shell system
var exec = require('child_process').exec;
var fs = require('fs');
var PythonShell = require('python-shell');
// post configuration
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

/*var ddpClient = new ddp({
  // All properties optional, defaults shown 
  host : "192.168.6.113",
  port : 3000,
  ssl  : false,
  autoReconnect : true,
  autoReconnectTimer : 500,
  maintainCollections : true,
  ddpVersion : '1',
  useSockJs: true,
  url: 'wss://192.168.6.113/websocket'
});

ddpClient.connect(function(error, wasReconnect) {
	if (error) {
		console.log('DDP connection error!');
		return;
	}

	if (wasReconnect) {
		console.log('Reestablishment of a connection.');
	}

	ddp.call('deletePosts', ['foo', 'bar'], function (err, result) {
      console.log('called function, result: ' + result);
  },function () {
    console.log('updated');
    console.log(ddpclient.collections.posts);
  });
});
*/

app.get('/', function(req, res){
	res.send('micro service');
});

// receive from meteor
app.post('/verify', function(req, res){
	// copy fingerprint
	console.log('verify');
	fs.writeFileSync('/home/pi/.fprint/prints/0002/00000000/7', fs.readFileSync('/home/pi/fingerRead/enrolled/'+req.body._id));
	// exec verify
	var child = exec('/home/pi/fingerRead/libfprint-0.6.0/examples/./ver');
	child.stdout.on('data', function(data) {
		// reponse
	  var verifyResponse = data.split('\n');
	  if(verifyResponse[1] == 'no match'){
			res.send('no match');
		}else if(verifyResponse[1] == 'match'){
			res.send('match');
		}else{
			res.send('error');
		}
	});
	child.stderr.on('data', function(data) {
	  //console.log('stdout: ' + data);
	});
	child.on('close', function(code) {
	  //console.log('closing code: ' + code);
	});
});

app.post('/enroll', function(req, res){
	// exec verify
	console.log('enroll');
	var child = exec('/home/pi/fingerRead/libfprint-0.6.0/examples/./en');
	child.stdout.on('data', function(data) {
	  var enrollResponse = data.split('\n');
	  // copy fingerprint
		fs.writeFileSync('/home/pi/fingerRead/enrolled/'+req.body._id, fs.readFileSync('/home/pi/.fprint/prints/0002/00000000/7'));
		// conver pgm to image
		var convertImage = exec('convert /home/pi/fingerRead/node_wrapper/enrolled.pgm /home/pi/fingerRead/enrolled/'+req.body._id+'.jpg');
		// response
		if(enrollResponse[1] == 'complete'){
			res.send('ok');
		}else{
			res.send('error');
		}
	});
	child.stderr.on('data', function(data) {
	  //console.log('stdout: ' + data);
	});
	child.on('close', function(code) {
	  //console.log('closing code: ' + code);
	}); 
});

app.post('/print', function(req, res){
	//send args to the script
	console.log('Print!');
 	var pyshell = new PythonShell('print2.py');
	// const action = data.action;
	var product = req.body.product;
	var quantity = req.body.quantity;
	var price = req.body.price;
	var internName = req.body.interno;
	Number.prototype.padLeft = function(base,chr){
    var  len = (String(base || 10).length - String(this).length)+1;
    return len > 0? new Array(len).join(chr || '0')+this : this;
	}
	var d = new Date,
    dformat = [(d.getMonth()+1).padLeft(),
               d.getDate().padLeft(),
               d.getFullYear()].join('/') +' ' +
              [d.getHours().padLeft(),
               d.getMinutes().padLeft(),
               d.getSeconds().padLeft()].join(':');
	console.log(product)
	console.log(quantity)
	console.log(price)
	console.log(internName)
	console.log(dformat)
	product = product.substring(0, product.length-1);
	quantity = quantity.substring(0, quantity.length-1);
	price = price.substring(0, price.length-1);
	// sends a message to the Python script via stdin
	pyshell.send(product);
	pyshell.send(quantity);
	pyshell.send(price);
	pyshell.send(dformat);
	pyshell.send(internName);
	pyshell.on('message', function (message) {
	  console.log(message);
	});
	// end the input stream and allow the process to exit
	pyshell.end();
    res.send('ok')
});

http.listen(8000, function(){
	console.log('8000');
});

